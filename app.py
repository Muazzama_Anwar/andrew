from webapp import app, Host, Port

if __name__ == "__main__":
    app.run(host=Host, port=Port, threaded=True, debug=True)
