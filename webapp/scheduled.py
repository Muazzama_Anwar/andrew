from webapp import app, celery
from .scrapper import ScrapData
from .processing import Processing
from .database import Database

from collections import OrderedDict


class Scheduled:

    @staticmethod
    @celery.task
    def scheduled():
        try:
            tokens = []
            addresses = ScrapData.filter_tokens()
            for addr in addresses[:5]:
                quantities = OrderedDict()
                resp = ScrapData.scrap('https://ethplorer.io/address/' + str(addr), str(addr))
                for i in range(2):
                    quantities = ScrapData.scrap_holders_data(
                        'https://ethplorer.io/address/' + str(addr) + '#tab=tab-holders&pageSize=100&holders=' + str(
                            i + 1), quantities)
                    if len(quantities) < 99:
                        break
                resp['averages'] = Processing.take_average(quantities)
                tokens.append(resp)
            Database.insert(tokens)
        except Exception as e:
            app.logger.exception(e)

