from webapp import app
from .processing import Processing

class Resources:

    @staticmethod
    def current_day():
        try:
            result_day = Processing.current_day()
            return result_day
        except Exception as e:
            app.logger.exception(e)

    @staticmethod
    def other_day(days, day_label):
        try:
            result_day = Processing.change_since(days, day_label)
            return result_day
        except Exception as e:
            app.logger.exception(e)

    @staticmethod
    def all_data():
        try:
            result_day = Processing.current_day()
            if result_day:
                result_day = Processing.customize_all(Processing.change_since(1, "Day 1"), result_day, "Day1")
                result_day = Processing.customize_all(Processing.change_since(2, "Day 2"), result_day, "Day2")
                result_day = Processing.customize_all(Processing.change_since(3, "Day 3"), result_day, "Day3")
                result_day = Processing.customize_all(Processing.change_since(7, "Week 1"), result_day, "Week1")
                result_day = Processing.customize_all(Processing.change_since(14, "Week 2"), result_day, "Week2")
                result_day = Processing.customize_all(Processing.change_since(21, "Week 3"), result_day, "Week3")
                result_day = Processing.customize_all(Processing.change_since(28, "Week 4"), result_day, "Week4")
                result_day = Processing.customize_all(Processing.change_since(42, "Week 6"), result_day, "Week6")
                result_day = Processing.customize_all(Processing.change_since(84, "Week 12"), result_day, "Week12")
                result_day = Processing.customize_all(Processing.change_since(168, "Week 24"), result_day, "Week24")
            return result_day
        except Exception as e:
            app.logger.exception(e)




