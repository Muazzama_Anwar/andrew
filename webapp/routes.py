import json

from webapp import app
from .resources import Resources
from flask import Response, render_template
from .scheduled import Scheduled

@app.route('/', methods=['GET'])
def index():

    result_day = Resources.all_data()
    return render_template("all_tokens.html", data=result_day)

@app.route('/testing', methods=['GET'])
def testing():
    Scheduled.scheduled()
    # result_day = Resources.all_data()
    return Response(json.dumps({"message": "Scrapping ended."}))


@app.route('/index', methods=['GET', 'POST'])
def day():
    result_day = Resources.current_day()
    return render_template("tokens.html", data=result_day)


@app.route('/index/day1', methods=['GET', 'POST'])
def day1():

    result_day = Resources.other_day(1, "Day 1")
    return render_template("tokens.html", data=result_day)


@app.route('/index/day2', methods=['GET', 'POST'])
def day2():

    result_day = Resources.other_day(2, "Day 2")
    return render_template("tokens.html", data=result_day)


@app.route('/index/day3', methods=['GET', 'POST'])
def day3():

    result_day = Resources.other_day(2, "Day 3")
    return render_template("tokens.html", data=result_day)


@app.route('/index/week1', methods=['GET', 'POST'])
def week1():

    result_day = Resources.other_day(7, "Week 1")
    return render_template("tokens.html", data=result_day)


@app.route('/index/week2', methods=['GET', 'POST'])
def week2():

    result_day = Resources.other_day(14, "Week 2")
    return render_template("tokens.html", data=result_day)


@app.route('/index/week3', methods=['GET', 'POST'])
def week3():

    result_day = Resources.other_day(21, "Week 3")
    return render_template("tokens.html", data=result_day)


@app.route('/index/week4', methods=['GET', 'POST'])
def week4():

    result_day = Resources.other_day(28, "Week 4")
    return render_template("tokens.html", data=result_day)


@app.route('/index/week6', methods=['GET', 'POST'])
def week6():

    result_day = Resources.other_day(42, "Week 6")
    return render_template("tokens.html", data=result_day)


@app.route('/index/week12', methods=['GET', 'POST'])
def week12():

    result_day = Resources.other_day(84, "Week 12")
    return render_template("tokens.html", data=result_day)


@app.route('/index/week24', methods=['GET', 'POST'])
def week24():

    result_day = Resources.other_day(168, "Week 24")
    return render_template("tokens.html", data=result_day)
