import sys

import yaml

from flask import Flask
from pymongo import MongoClient

from celery import Celery
from celery.schedules import crontab

app = Flask(__name__)

try:
    config = yaml.load(open("config.yml"))

    # importing configurable variables
    Host = config['SERVER']['host']  # Server Host
    Port = config['SERVER']['port']  # Server Port
    CeleryConf = config['CELERY']

    # importing database connection settings
    db = MongoClient(config['DATABASE']['uri'])
    db = db[config['DATABASE']['db']]
    db.authenticate(config['DATABASE']['username'], config['DATABASE']['password'])
    token_details = db[config['DATABASE']['collection1']]

    # celery configurations
    app.config['CELERY_BROKER_URL'] = CeleryConf['mq_url']
    app.config['result_backend'] = CeleryConf['mq_backend']
    app.config['broker_pool_limit'] = None

    # register tasks
    app.config['imports'] = CeleryConf['tasks']

    # initialize celery
    celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])

    # schedule task
    celery.conf.beat_schedule = {
        'scrap-every-midnight': {
            'task': 'webapp.scheduled.scheduled',
            'schedule': crontab(minute=0, hour='*/1')
        },
    }

    # update configurations
    celery.conf.update(app.config)

    # application blueprints registration
    import webapp.routes

except Exception as e:
    app.logger.info("Error occurred while parsing configurations and blueprint registration.")
    app.logger.exception(e)
    sys.exit(1)
