import json, os

from webapp import app
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
class ScrapData:

    @staticmethod
    def filter_tokens():
        try:
            file = open(ROOT_DIR + '/static/token.json')
            file = json.load(file)
            addresses = [f['address'] for f in file]
            return addresses
        except Exception as e:
            app.logger.exception(e)

    @classmethod
    def scrap(cls, url, contract):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--dns-prefetch-disable')
        options.add_argument('--no-sandbox')
        driver = webdriver.Chrome('/usr/bin/chromedriver', chrome_options=options)
        driver.set_page_load_timeout(10000)

        try:
            data = dict()

            driver.get(url)

            soup = BeautifulSoup(driver.page_source, 'lxml')

            data['contract'] = contract

            token_details = soup.find('div', {'id': 'address-token-details'})

            symbol = token_details.find('td', {'id': 'address-token-symbol'}).text
            data['symbol'] = symbol

            price = token_details.find('td', {'id': 'address-token-price'})
            price = price.find('span').text.replace("$", "") if price.find('span') else ""
            data['price'] = float(price) if price else 0

            supply = token_details.find('td', {'id': 'address-token-totalSupply'}).text.split(" ", 1)[0].replace(",", "")
            data['supply'] = float(supply) if supply else 0

            marketcap = float(data['price']*data['supply'])
            data['marketcap'] = marketcap
            return data
        except Exception as e:
            app.logger.exception(e)
            cls.scrap(url, contract)

    @classmethod
    def scrap_holders_data(cls, url, quantities):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--dns-prefetch-disable')
        options.add_argument('--no-sandbox')
        driver = webdriver.Chrome('/usr/bin/chromedriver', chrome_options=options)
        driver.set_page_load_timeout(10000)
        try:

            driver.get(url)

            soup = BeautifulSoup(driver.page_source, 'lxml')

            token_holders = soup.find('div', {'id': 'address-token-holders'})
            holders_table = token_holders.find('table', {'class': 'table'})
            rows = holders_table.find_all('tr', limit=100)
            for row in rows[:len(rows)-1]:
                columns = row.find_all('td')
                if columns[0].text and columns[3].text:
                    quantities[columns[0].text] = float(columns[3].text.split(" ")[0].replace(",", ""))
            return quantities
        except Exception as e:
            app.logger.exception(e)
            cls.scrap_holders_data(url, quantities)
