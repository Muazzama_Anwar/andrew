from webapp import token_details, app
from uuid import uuid4
from datetime import datetime, timedelta
from dateutil import parser


class Database:
    @staticmethod
    def insert(details):
        try:
            token_details.insert(
                {
                    "_id": str(uuid4()),
                    "date":datetime.today().strftime("%Y-%m-%d %H:%M:%S"),
                    "details": details
                })
        except Exception as e:
            app.logger.exception(e)

    @staticmethod
    def current_day():
        try:
            date = parser.parse(datetime.today().date().strftime("%Y-%m-%d %H:%M:%S"))
            next_day = date + timedelta(days=1)
            cursor = token_details.find_one({"date": {'$gte': date.strftime("%Y-%m-%d %H:%M:%S"),
                                                      '$lt': next_day.strftime("%Y-%m-%d %H:%M:%S")}})
            return cursor
        except Exception as e:
            app.logger.exception(e)

    @staticmethod
    def days(days):
        try:
            date = parser.parse(datetime.today().date().strftime("%Y-%m-%d %H:%M:%S")) - timedelta(days=days)
            next_day = date + timedelta(days=1)
            cursor = token_details.find_one({"date": {'$gte': date.strftime("%Y-%m-%d %H:%M:%S"),
                                                      '$lt': next_day.strftime("%Y-%m-%d %H:%M:%S")}})
            return cursor
        except Exception as e:
            app.logger.exception(e)
