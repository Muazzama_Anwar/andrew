from webapp import app
from .database import Database

from collections import OrderedDict
from itertools import islice


class Processing:

    @staticmethod
    def chunks(data, size):
        it = iter(data)
        for i in range(0, len(data), size):
            yield [data[k] for k in islice(it, size)]

    @staticmethod
    def chunk_results(data, size):
        it = iter(data)
        for i in range(0, len(data), size):
            results = {k: data[k] for k in islice(it, size)}
            yield sorted(results.items())

    @staticmethod
    def take_average(quantities):
        try:
            averages = OrderedDict()
            chunks = []
            for item in Processing.chunks(quantities, 10):
                chunks.append(item)
            for c in range(len(chunks)):
                averages[str(c)] = sum(chunks[c]) / len(chunks[c])
            return averages
        except Exception as e:
            app.logger.exception(e)

    @staticmethod
    def current_day():
        result_data = []
        record = Database.current_day()
        if record:
            for data in record['details']:
                result = {}
                result['symbol'] = data['symbol']
                result['marketcap'] = round(data['marketcap'], 3)
                result['price'] = round(data['price'], 3)
                result_avg = dict()
                averages = sorted({int(k): v for k, v in data['averages'].items()}.items())
                for key, value in averages:
                    result_avg[key] = {
                        "average": round(value, 3),
                        "change": "-"
                    }
                result['data'] = result_avg
                result_data.append(result)
            result_data = Processing.customize(result_data, "Current")
        return result_data

    @staticmethod
    def change_since(days, label):
        result_data = []
        current = Database.current_day()
        since_day = Database.days(days)
        if current:
            if since_day:
                for curr, sin in zip(current['details'], since_day['details']):
                    result = {}
                    result_avg = dict()
                    result['symbol'] = curr['symbol']
                    result['marketcap'] = round(sin['marketcap'], 3)
                    result['price'] = round(sin['price'], 3)
                    curr_averages = sorted({int(k): v for k, v in curr['averages'].items()}.items())
                    sin_averages = sorted({int(k): v for k, v in curr['averages'].items()}.items())
                    for (key, value), (key2, value2) in zip(curr_averages, sin_averages):
                        result_avg[key] = {
                            "average": round(value2, 3),
                            "change": Processing.change_percentage(value2, value)
                        }
                    result['data'] = result_avg
                    result_data.append(result)
                result_data = Processing.customize(result_data, label)
                return result_data
            else:
                return result_data
        else:
            print("current day data still processing")

    @staticmethod
    def change_percentage(new_number, original_number):
        change = new_number - original_number
        percentage = (change/original_number)*100
        return float(percentage)

    @staticmethod
    def customize(result, label):
        for data in result:
            chunks = []
            for item in Processing.chunk_results(data['data'], 10):
                chunks.append(item)
            data['data'] = chunks
            data['day'] = label
        return result

    @staticmethod
    def customize_all(new_result, orignal_result, label):
        if new_result:
            for data1, data2 in zip(new_result, orignal_result):
                data2[label] = data1['data'] if data1 else []
        else:
            for data in orignal_result:
                data[label] = []
        return orignal_result
